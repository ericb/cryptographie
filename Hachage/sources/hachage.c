/*  Copyright Eric Bachard  01 juin 2016 */

/* licence GPL V2 */


#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <linux/fd.h>
#include <stdbool.h>

/* D'après POSIX.1-2001 */

/* autres implémentations */
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include "getch.h"
#include "efface_ecran.h"
#include "saisie.h"
#include "utils.h"
#include "strings_utils.h"
#include "constants.h"

static char chaine_a_hacher[STRING_SIZE_MAX] = "";
static int length = STRING_SIZE_MAX;
static int * p_length = &length;

static void redo(void)
{
    efface_ecran();

    clear_string(chaine_a_hacher);

    new_word(chaine_a_hacher, p_length);

    fprintf(stdout, "Le hash de la chaine ou du mot entré(e) vaut : %d \n", hash_func(chaine_a_hacher, p_length));


    fprintf( stdout, "\nAppuyer sur une touche pour compléter la chaîne, ou sur la touche échap (ESC) pour quitter.");
    fprintf( stdout, "\nPour saisir une nouvelle chaîne à chiffrer ou déchiffrer, appuyer sur la touche \" # \"\n\n");
}

int main(void)
{
    bool bQuit = false;
    int c = 0;

    redo();

    do 
    {
        do
        {
            c = 0;
            c = (int)getch();
        }
        while ((c < CMIN) || (c > CMAX));

        switch (c)
        {
            case CHAR_ESC:
                bQuit = true;
            break;

            case CHAR_SHARP:
                redo();
            break;

            default:
            break;
        }
    } while (false == bQuit);


    return EXIT_SUCCESS;
}

