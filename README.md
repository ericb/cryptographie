Ceci est un brouillon, et évoluera probablement, quand j'aurai un peu de temps.

Merci de reporter les problèmes rencontrés avec le code en créant une "issue" (un rapport de bug / une question / une proposition).
La courtoisie est évidemment appréciée pour ce type de rapport.
