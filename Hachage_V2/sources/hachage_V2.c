/*  Copyright Eric Bachard  01 juin 2016 */

/* licence GPL V2 */


#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <linux/fd.h>
#include <stdbool.h>

/* D'après POSIX.1-2001 */

/* autres implémentations */
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include "getch.h"
#include "efface_ecran.h"
#include "saisie.h"
#include "utils.h"
#include "strings_utils.h"
#include "constants.h"

static char chaine_a_hacher[STRING_SIZE_MAX] = "";
static int length = STRING_SIZE_MAX;
static int * p_length = &length;
static int hash = 0;
static int * p_hash = &hash;


static void display_hash(void)
{
    efface_ecran();

    fprintf( stdout, "Entrer les lettres pour afficher le résultat de la fonction de hachage \n");
    fprintf( stdout, "Ou appuyer sur la touche échap (ESC) pour quitter.\n\n");

    fprintf(stdout, "Le hash de la chaine saisie vaut : %d \n\n", hash);
    fprintf(stdout, "La chaîne saisie contient : %s \n", chaine_a_hacher);
}


int main(void)
{
    clear_string(chaine_a_hacher);
    efface_ecran();

    bool bQuit = false;
    int c = 0;

    display_hash();

    while (false == bQuit)
    {
        display_hash();

        do
        {
            c = 0;
            c = (int)getch();
        }
        while ((c < CMIN) || (c > CMAX));

#ifdef DEBUG
        fprintf( stdout, "Caractère saisi : %c \n", c);
        fprintf( stdout, "Valeur hexadécimale : 0x%X \n", c);
        fprintf( stdout, "Valeur décimale : %d \n", c);
#endif
        // Improve me (missing characters ...)
        if (   ((c > 0x40) && (c < 0x5B))
             ||((c > 0x60) && (c < 0x7B))
             ||((c > 0x30) && (c < 0x39))
             || (c == CHAR_SPACE))
        {
            hash_letter(c, p_hash);
            add_letter_to_string(c, chaine_a_hacher, p_length);
            c = CHAR_NEW_ASCII_LETTER;
        }

        switch (c)
        {
            case CHAR_ESC:
                bQuit = true;
            break;

            case CHAR_SHARP:
                clear_string(chaine_a_hacher);
                length = 0;
                hash = 0;
            break;

            case CHAR_BACKSPACE:
                dehash_letter(chaine_a_hacher[length -1], p_hash);
                remove_last_letter_in_string(chaine_a_hacher, p_length);
            break;

            case CHAR_NEW_ASCII_LETTER:
            default:
            break;
        }
    }
    return EXIT_SUCCESS;
}

